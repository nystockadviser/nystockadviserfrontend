export class SearchResult {
  private _symbol: string;
  private _name: string;
  private _type: string;
  private _region: string;
  private _marketOpen: string;
  private _marketClose: string;
  private _timezone: string;
  private _currency: string;
  private _matchScore: string;

  constructor(symbol: string, name: string, type: string, region: string, marketOpen: string, marketClose: string, timezone: string, currency: string, matchScore: string) {
    this._symbol = symbol;
    this._name = name;
    this._type = type;
    this._region = region;
    this._marketOpen = marketOpen;
    this._marketClose = marketClose;
    this._timezone = timezone;
    this._currency = currency;
    this._matchScore = matchScore;
  }

  get symbol(): string {
    return this._symbol;
  }

  set symbol(value: string) {
    this._symbol = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get region(): string {
    return this._region;
  }

  set region(value: string) {
    this._region = value;
  }

  get marketOpen(): string {
    return this._marketOpen;
  }

  set marketOpen(value: string) {
    this._marketOpen = value;
  }

  get marketClose(): string {
    return this._marketClose;
  }

  set marketClose(value: string) {
    this._marketClose = value;
  }

  get timezone(): string {
    return this._timezone;
  }

  set timezone(value: string) {
    this._timezone = value;
  }

  get currency(): string {
    return this._currency;
  }

  set currency(value: string) {
    this._currency = value;
  }

  get matchScore(): string {
    return this._matchScore;
  }

  set matchScore(value: string) {
    this._matchScore = value;
  }
}
