import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SearchResult} from "./search-result";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StockSearchService {

  constructor(private http: HttpClient) { }

  stockSearch(keyword: string): Observable<SearchResult | null> {
    const url = 'http://localhost:8000/search/${keyword}';
    const params = new HttpParams()
      .set('keyword', keyword);

    return this.http
      .get<SearchResult>(url, {
        observe: 'response',
        params
      })
      .pipe(
        map((res: Response) => {
          return res.body;
        })
      );
  }
}
